# docker-django

The base Docker image for my Django projects

This is basically just a thin wrapper around `python:3` to make the environment
inside the container more me-friendly.


## Alpine

* Adds Bash, Poetry, and ipython
* Tweaks the iPython config to not ask for confirmation on exit.


## Debian

* Adds Bash, Poetry, ipython, and net-tools
* Tweaks the iPython config to not ask for confirmation on exit.
